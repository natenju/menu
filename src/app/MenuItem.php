<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 8:59 PM
 * @Updated: 3/10/2019 8:59 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\Menu\App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Natenju\School\Facades\School as SchoolFacade;

class MenuItem extends Model {
    protected $table = 'menu_items';
    
    protected $guarded = [];
    
    public function children() {
        return $this->hasMany(SchoolFacade::modelClass('MenuItem'), 'parent_id')
                    ->with('children');
    }
    
    public function menu() {
        return $this->belongsTo(SchoolFacade::modelClass('Menu'));
    }
    
    public function link($absolute = FALSE) {
        return $this->prepareLink($absolute, $this->route, $this->parameters, $this->url);
    }
    
    protected function prepareLink($absolute, $route, $parameters, $url) {
        if ( is_null($parameters) ) {
            $parameters = [];
        }
        
        if ( is_string($parameters) ) {
            $parameters = json_decode($parameters, TRUE);
        } elseif ( is_object($parameters) ) {
            $parameters = json_decode(json_encode($parameters), TRUE);
        }
        
        if ( !is_null($route) ) {
            if ( !Route::has($route) ) {
                return '#';
            }
            
            return route($route, $parameters, $absolute);
        }
        
        if ( $absolute ) {
            return url($url);
        }
        
        return $url;
    }
    
    public function getParametersAttribute() {
        return json_decode($this->attributes['parameters']);
    }
    
    public function setParametersAttribute($value) {
        if ( is_array($value) ) {
            $value = json_encode($value);
        }
        
        $this->attributes['parameters'] = $value;
    }
    
    public function setUrlAttribute($value) {
        if ( is_null($value) ) {
            $value = '';
        }
        
        $this->attributes['url'] = $value;
    }
    
    /**
     * Return the Highest Order Menu Item.
     *
     * @param number $parent (Optional) Parent id. Default null
     *
     * @return number Order number
     */
    public function highestOrderMenuItem($parent = NULL) {
        $order = 1;
        
        $item = $this->where('parent_id', '=', $parent)
                     ->orderBy('order', 'DESC')
                     ->first();
        
        if ( !is_null($item) ) {
            $order = intval($item->order) + 1;
        }
        
        return $order;
    }
}