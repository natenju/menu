<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 8:37 PM
 * @Updated: 3/10/2019 8:37 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\Menu\App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Natenju\School\Facades\School as SchoolFacade;

class Menu extends Model {
    protected $table = 'menus';
    
    protected $guarded = [];
    
    public function items() {
        return $this->hasMany(SchoolFacade::modelClass('MenuItem'));
    }
    
    public function parent_items() {
        return $this->hasMany(SchoolFacade::modelClass('MenuItem'))
                    ->whereNull('parent_id');
    }
    
    /**
     * Display menu.
     *
     * @param string      $menuName
     * @param string|null $type
     * @param array       $options
     *
     * @return string
     */
    public static function display($menuName, $type = NULL, array $options = []) {
        // GET THE MENU - sort collection in blade
        $menu = static::where('name', '=', $menuName)
                      ->with(
                          [
                              'parent_items.children' => function ($q) {
                                  $q->orderBy('order');
                              },
                          ]
                      )
                      ->first();
        
        // Check for Menu Existence
        if ( !isset($menu) ) {
            return FALSE;
        }
        
        // Convert options array into object
        $options = (object)$options;
        
        // Set static vars values for admin menus
        if ( in_array($type, ['admin', 'admin_menu']) ) {
            $permissions = SchoolFacade::model('Permission')->all();
            $prefix = trim(route('school.dashboard', [], FALSE), '/');
            $user_permissions = NULL;
            
            if ( !Auth::guest() ) {
                $user = SchoolFacade::model('User')->find(Auth::id());
                $user_permissions = $user->role ? $user->role->permissions->pluck('key')->toArray() : [];
            }
            
            $options->user = (object)compact('permissions', 'prefix', 'user_permissions');
            
            // change type to blade template name - TODO funky names, should clean up later
            $type = 'school::menu.' . $type;
        } else {
            if ( is_null($type) ) {
                $type = 'school::menu.default';
            } elseif ( $type == 'bootstrap' && !view()->exists($type) ) {
                $type = 'school::menu.bootstrap';
            }
        }
        
        if ( !isset($options->locale) ) {
            $options->locale = app()->getLocale();
        }
        
        $items = $menu->parent_items->sortBy('order');
        
        if ( $type === '_json' ) {
            return $items;
        }
        
        return new \Illuminate\Support\HtmlString(
            \Illuminate\Support\Facades\View::make($type, ['items' => $items, 'options' => $options])->render()
        );
    }
}