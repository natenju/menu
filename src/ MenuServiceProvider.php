<?php

/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/9/2019 9:59 PM
 * @Updated: 3/9/2019 9:59 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\Menu;


use Illuminate\Support\ServiceProvider;

/**
 * Class  MenuServiceProvider
 *
 * @package Natenju\Menu
 */
class MenuServiceProvider extends ServiceProvider {
    
    protected $policies = [];
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register() { }
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
    
    }
}
